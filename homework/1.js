/*

  Задание написать простой слайдер.

    Есть массив с картинками из которых должен состоять наш слайдер.
    По умолчанию выводим первый элмемнт нашего слайдера в блок с id='slider'
    ( window.onload = func(){...} // window.addEventListener('load', function(){...}) )
    По клику на PrevSlide/NextSlideShow показываем предыдущий или следующий сладй соответствено.

    Для этого необходимо написать 4 функции которые будут:

    1. Срабатывать на событие load обьекта window и загружать наш слайд по умолчанию.
    2. RenderImage -> Очищать текущий контент блока #slider. Создавать нашу картинку и через метод аппенд чайлд вставлять её в слайдер.
    3. NextSlideShow -> По клику на NextSilde передавать currentPosition текущего слайда + 1 в функцию рендера
    4. PrevSlide -> Тоже самое что предыдущий вариант, но на кнопку PrevSlide и передавать currentPosition - 1
      + бонус сделать так что бы при достижении последнего и первого слада вас после кидало на первый и последний слайд соответственно.
      + бонсу анимация появления слайдов через навешивание дополнительных стилей

*/

  var ourSliderImages = ['images/cat1.jpg', 'images/cat2.jpg', 'images/cat3.jpg', 'images/cat4.jpg', 'images/cat5.jpg', 'images/cat6.jpg', 'images/cat7.jpg', 'images/cat8.jpg'];
  var currentPosition = 0;
  var sliderDiv = document.getElementById('slider');
  sliderDiv.className = 'slider';
  var nextSlideShowButt = document.getElementById('NextSilde');
  var prevSlideButt = document.getElementById('PrevSilde');
  var currentSlide;

  // Функция появления слайда
  function slideAppear(elem) {
    if (currentSlide != undefined) {
      sliderDiv.removeChild(currentSlide);
      currentSlide = document.createElement('img');
      currentSlide.setAttribute('src', ourSliderImages[elem]);
    } else {
      currentSlide = document.createElement('img');
      currentSlide.setAttribute('src', ourSliderImages[elem]);
    }
  }

  slideAppear(currentPosition);
  // currentSlide.className = 'loadedSlide';
  currentSlide.className = 'loadedSlideDone';
  sliderDiv.appendChild(currentSlide);

  nextSlideShowButt.addEventListener('click', function() {
    // debugger;
    if (currentPosition == ourSliderImages.length - 1) { currentPosition = 0}
    else { currentPosition++; }
    // currentPosition++;
    slideAppear(currentPosition);
    // currentSlide.className = 'loadedSlide';
    currentSlide.className = 'slide-transform';
    sliderDiv.appendChild(currentSlide);
    // currentSlide.classList.remove('loadedSlideShowLeft');
    function nextSlideShowR() {
      currentSlide.classList.add('showing-transform');
    }
    var slideInterval = setInterval(nextSlideShowR, 50);
  })
    
  prevSlideButt.addEventListener('click', function() {
    // debugger;
    if (currentPosition == 0) { currentPosition = ourSliderImages.length - 1}
    else { currentPosition--; }
    slideAppear(currentPosition);
    // currentSlide.className = 'loadedSlideLeft';
    currentSlide.className = 'slide-transform';
    sliderDiv.appendChild(currentSlide);
    // currentSlide.classList.remove('loadedSlideShowRight');
    function nextSlideShowL() {
      currentSlide.classList.add('showing-transform');
    }
    var slideInterval = setInterval(nextSlideShowL, 50);
  })
    
    
    
    
  